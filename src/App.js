import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

	class App extends Component {
	constructor(props){
		super(props);
		this.id = 'desarrolloId';
		this.ctx = null;
		this.color= "rgba(0,0,0,.2)";
		this.canvas = null;
		this.xAnt = 0;
		this.yAnt = 0;
	}
	componentDidMount(){
		this.canvas = document.getElementById(this.id);

		/**
		 * NEW CONFIGURATION
		 */
		this.ctx = this.canvas.getContext('2d');
		console.log(this.ctx);
		this.ctx.lineWidth   = 1;
		this.ctx.strokeStyle = this.color;
		this.ctx.fillStyle   = this.color;
		this.canvas.width = 400;
		this.canvas.height = 300;
		this.total = 0;		
	}	
	/**
	 * Draw de signature
	 * @param {*} evento 
	 */
	drawnwingLine(evento){
		
		//hallamos la posición: page x-y es con respecto al ratón,
		//offset con respecto al borde del canvas
		var rect = this.canvas.getBoundingClientRect();		
		let posX = Math.floor(evento.touches[0].pageX - rect.left);
		let posY = Math.floor(evento.touches[0].pageY - rect.top);				
		let x = posX;
		let y = posY;

		if (this.xAnt == 0 || this.yAnt == 0) {
			this.xAnt = x;
			this.yAnt = y;
		}
		//color de la línea y del relleno
		//(aunque ahora no hay relleno)
		this.ctx.beginPath();
		//nos movemos a la posición actual
		this.ctx.moveTo(x, y);
		//Hacemos una línea a la posición anterior
		this.ctx.lineTo(this.xAnt, this.yAnt);
		//pintamos la línea
		this.ctx.stroke();
		//guardamos los
		this.xAnt = x;
		this.yAnt = y;
		
	}
	/**
	 * evit begin pointer in the last position
	 * @param {*} event 
	 */
	clean (event)  {
		this.xAnt = 0;
		this.yAnt = 0;
	}

	render() {

		return (
			<div>
				<canvas id={this.id} onTouchMove={this.drawnwingLine.bind(this)} onTouchEnd={this.clean.bind(this)}></canvas>				
			</div>
			
		);
	}
	}

	export default App;
